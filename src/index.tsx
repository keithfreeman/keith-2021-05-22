import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            REACT_APP_GROUPS: string;
            REACT_APP_INCREMENTS: string;
        }
    }
}

ReactDOM.render(
  <React.StrictMode>
    <App initialGroups={process.env.REACT_APP_GROUPS.split(",")} increments={process.env.REACT_APP_INCREMENTS.split(",").map((increment) => parseFloat(increment))} />
  </React.StrictMode>,
  document.getElementById('root')
);
