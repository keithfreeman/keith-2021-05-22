import React from 'react';
import './App.css';
import {LRUBuffer, Websocket, WebsocketBuilder} from "websocket-ts";
import OrderBook from "./components/OrderBook";
import {LevelDelta, LevelIt, Levels, PriceSize} from "./components/Levels";

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            REACT_APP_FEED_URL: string;
            REACT_APP_INCREMENTS: string;
            REACT_APP_PRODUCTS: string;
        }
    }
}

interface Book {
    aLevels: Levels;
    asks: PriceSize;
    bLevels: Levels;
    bids: PriceSize;
    group: number;
}

interface Props {
    initialGroups: string[];
    increments: number[];
}

interface State {
    connectionStatus: number;
    orderbooks: {
        [id: string]: Book
    };
    products: string[];
    socket: Websocket | undefined;
    subscriptions: string[];
}

export default class App extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            connectionStatus: 0,
            orderbooks: {},
            products: process.env.REACT_APP_PRODUCTS.split(","),
            socket: undefined,
            subscriptions: []
        }
    }

    componentDidMount() {
        let socket =  new WebsocketBuilder(process.env.REACT_APP_FEED_URL)
            .withBuffer(new LRUBuffer(1000))
            .onOpen(() => { this.handleOpen(); })
            .onClose(() => { this.handleClosure(); })
            .onError(() => { this.handleError(); })
            .onRetry(() => { this.handleRetry(); })
            .onMessage((i, e) => { this.handleMessage(e); })
            .build();

        this.setState({
            socket
        });

        socket.send(JSON.stringify({"event" : "subscribe", "feed" : "book_ui_1", "product_ids" : this.state.products}));
    }

    componentWillUnmount() {
        this.state.socket && this.state.socket.close();
    }

    getConnectionState(): string {
        switch (this.state.connectionStatus) {
            case 0:
                return 'Connecting'

            case 1:
                return 'Connected';

            case 2:
                return 'Disconnected';

            case 3:
                return 'Retrying';

            default:
                return 'Errored';
        }
    }

    handleClosure() {
        this.setState({ connectionStatus: 2, subscriptions: [] });
    }

    handleError() {
        this.setState({ connectionStatus: 4, subscriptions: [] });
    }

    handleMessage(message: MessageEvent) {
        let data = JSON.parse(message.data);

        if (data.event) {
            if (data.event === 'info') {
                return;
            }

            if (data.event === 'subscribed') {
                data.product_ids.forEach((productId: string) => {
                    if (this.state.subscriptions.indexOf(productId) === -1) {

                        this.setState(prevState => {
                            let obs = prevState.orderbooks;
                            let index = this.state.products.indexOf(productId);

                            obs[productId] = {
                                aLevels: {},
                                asks: {},
                                bids: {},
                                bLevels: {},
                                group: parseFloat(this.props.initialGroups[index])
                            };

                            return {
                                subscriptions: [...prevState.subscriptions, productId]
                            };
                        });
                    }
                });

                return;
            }
        }

        if (data.feed === 'book_ui_1_snapshot') {
            if (this.state.subscriptions.indexOf(data.product_id) === -1 || !this.state.orderbooks[data.product_id]) {
                return;
            }

            this.setState(prevState => {
                let obs = prevState.orderbooks;
                let asks: PriceSize = {};
                let bids: PriceSize = {};

                data.asks.forEach((one: [number, number]) => {
                    asks[one[0] + ''] = one;
                });

                data.bids.forEach((one: [number, number]) => {
                    bids[one[0] + ''] = one;
                });

                obs[data.product_id] = {
                    aLevels: LevelIt(asks, prevState.orderbooks[data.product_id].group, true),
                    asks,
                    bids,
                    bLevels: LevelIt(bids, prevState.orderbooks[data.product_id].group, false),
                    group: prevState.orderbooks[data.product_id].group
                };

                return {
                    orderbooks: obs
                }
            })
        }

        if (data.feed === 'book_ui_1') {
            if (this.state.subscriptions.indexOf(data.product_id) === -1 || !this.state.orderbooks[data.product_id]) {
                return;
            }

            this.setState(prevState => {
                let obs = prevState.orderbooks;
                let newA = LevelDelta(obs[data.product_id].asks, data.asks);
                let newB = LevelDelta(obs[data.product_id].bids, data.bids);

                obs[data.product_id] = {
                    aLevels: LevelIt(newA, prevState.orderbooks[data.product_id].group, true),
                    asks: newA,
                    bids: newB,
                    bLevels: LevelIt(newB, prevState.orderbooks[data.product_id].group, false),
                    group: prevState.orderbooks[data.product_id].group
                };

                return {
                    orderbooks: obs
                };
            })
        }
    }

    handleOpen() {
        this.setState({ connectionStatus: 1 });
    }

    handleRetry() {
        this.setState({ connectionStatus: 3, subscriptions: [] });
    }

    onGroupChange(product: string, up: boolean): void {
        if (this.state.orderbooks[product] === undefined) {
            return;
        }

        let current = this.state.orderbooks[product].group;
        let index = this.props.increments.indexOf(current);
        let change: number = 0;

        if (up) {
            if (index < this.props.increments.length - 1) {
                change = this.props.increments[index + 1];
            }
        } else {
            if (index > 0) {
                change = this.props.increments[index - 1];
            }
        }

        if (change !== 0) {
            this.setState(prevState => {
                let obs = prevState.orderbooks;

                obs[product] = {
                    aLevels: obs[product].aLevels,
                    asks: obs[product].asks,
                    bids: obs[product].bids,
                    bLevels: obs[product].bLevels,
                    group: change
                }

                return {
                    orderbooks: obs
                };
            })
        }
    }

    render() {
        return (
            <div className="App">
                <div className="App-menu">
                    <div className="App-name">Sigma 36</div>
                    <div className={ this.state.connectionStatus === 1 ? 'Feed-status Feed-on' : 'Feed-status Feed-off' }>
                        Feed { this.getConnectionState() }
                    </div>
                </div>
                <div className="App-main">
                    <div className="App-orderbooks">
                        {
                            Object.keys(this.state.orderbooks).map((product) =>
                                <OrderBook
                                    key={product}
                                    asks={this.state.orderbooks[product].aLevels}
                                    bids={this.state.orderbooks[product].bLevels}
                                    group={this.state.orderbooks[product].group}
                                    minGroup={this.props.increments[0]}
                                    maxGroup={this.props.increments[this.props.increments.length - 1]}
                                    onGroupChange={this.onGroupChange.bind(this)}
                                    product={product}
                                />
                            )
                        }
                    </div>
                </div>
                <div className="App-logo"><img src="logo.png" alt="Sigma 36 logo"/></div>
            </div>
        );
    }
}
