import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      REACT_APP_INCREMENTS: string;
      REACT_APP_PRODUCTS: string;
    }
  }
}

test('renders page and elements', () => {
  render(<App initialGroups={["10"]} increments={[.25,.5]}/>);

  let element = screen.getByAltText('Sigma 36 logo');
  expect(element).toBeInTheDocument();

  element = screen.getByText(/Feed/);
  expect(element).toBeInTheDocument();
});

