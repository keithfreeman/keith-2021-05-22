export interface PriceSize {
    [id: string]: [number, number]
}

export interface Levels {
    [id: string]: {
        size: number;
        total: number;
    }
}

export function LevelDelta(current: PriceSize, delta: Array<[number, number]>): PriceSize {
    delta.forEach((one) => {
        if (one[1] === 0) {
            delete current[one[0]];
        } else {
            current[one[0] + ''] = one;
        }
    });

    return current;
}

export function LevelIt(all: PriceSize, group: number, roundUp: boolean): Levels {
    let levels: Levels = {};
    let points: number[] = [];
    let totalSize = 0;

    Object.keys(all).forEach((one) => {
        let level = all[one][0] / group;
        level = roundUp ? Math.ceil(level) : Math.floor(level);
        level *= group;

        if (levels[level + ''] === undefined) {
            points.push(level);
            levels[level + ''] = {
                size: 0,
                total: 0
            }
        }

        levels[level + ''].size += all[one][1];
        totalSize += all[one][1];
    });

    if (roundUp) {
        points.sort().reverse().forEach((point) => {
            levels[point + ''].total = totalSize;
            totalSize -= levels[point + ''].size;
        });
    } else {
        points.sort().forEach((point) => {
            levels[point + ''].total = totalSize;
            totalSize -= levels[point + ''].size;
        });
    }

    return levels;
}
