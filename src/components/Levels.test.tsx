import * as levels from './Levels';

test('groups ask price levels correctly', () => {
    let expected: levels.Levels = {
        "10" : { size: 25, total: 25},
        "11" : { size: 50, total: 75},
        "12" : { size: 50, total: 125},
        "13" : { size: 50, total: 175},
        "14" : { size: 50, total: 225},
        "15" : { size: 50, total: 275},
        "16" : { size: 25, total: 300},
    }


    let asks: levels.PriceSize = {
        "10"    : [10, 25],
        "10.25" : [10.25, 25],
        "11"    : [11, 25],
        "11.25" : [11.25, 25],
        "12"    : [12, 25],
        "12.25" : [12.25, 25],
        "13"    : [13, 25],
        "13.25" : [13.25, 25],
        "14"    : [14, 25],
        "14.25" : [14.25, 25],
        "15"    : [15, 25],
        "15.25" : [15.25, 25]
    }

    let askLevels = levels.LevelIt(asks, 1, true);

    expect(askLevels).toEqual(expected);
});

test('groups bid price levels correctly', () => {
    let expected: levels.Levels = {
        "10" : { size: 50, total: 300},
        "11" : { size: 50, total: 250},
        "12" : { size: 50, total: 200},
        "13" : { size: 50, total: 150},
        "14" : { size: 50, total: 100},
        "15" : { size: 50, total: 50},
    }


    let bids: levels.PriceSize = {
        "10"    : [10, 25],
        "10.25" : [10.25, 25],
        "11"    : [11, 25],
        "11.25" : [11.25, 25],
        "12"    : [12, 25],
        "12.25" : [12.25, 25],
        "13"    : [13, 25],
        "13.25" : [13.25, 25],
        "14"    : [14, 25],
        "14.25" : [14.25, 25],
        "15"    : [15, 25],
        "15.25" : [15.25, 25]
    }

    let bidLevels = levels.LevelIt(bids, 1, false);

    expect(bidLevels).toEqual(expected);
});

test('deltas price levels correctly', () => {
    let expected: levels.PriceSize = {
        "10"    : [10, 25],
        "10.25" : [10.25, 25],
        "11.25" : [11.25, 25],
        "12"    : [12, 25],
        "12.25" : [12.25, 25],
        "13"    : [13, 25],
        "13.25" : [13.25, 25],
        "14"    : [14, 25],
        "14.25" : [14.25, 30],
        "15"    : [15, 25],
        "15.25" : [15.25, 25],
        "16"    : [16, 25]
    }


    let bids: levels.PriceSize = {
        "10"    : [10, 25],
        "10.25" : [10.25, 25],
        "11"    : [11, 25],
        "11.25" : [11.25, 25],
        "12"    : [12, 25],
        "12.25" : [12.25, 25],
        "13"    : [13, 25],
        "13.25" : [13.25, 25],
        "14"    : [14, 25],
        "14.25" : [14.25, 25],
        "15"    : [15, 25],
        "15.25" : [15.25, 25]
    }

    let bidsNew: Array<[number, number]> = [
        [11, 0],
        [14.25, 30],
        [16, 25]
    ]


    let bidLevels = levels.LevelDelta(bids, bidsNew);

    expect(bidLevels).toEqual(expected);
});
