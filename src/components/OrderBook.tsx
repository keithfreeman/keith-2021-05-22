import React from 'react';
import './OrderBook.css';
import {Levels} from './Levels';

interface Props {
    asks: Levels;
    bids: Levels;
    group: number;
    minGroup: number;
    maxGroup: number;
    onGroupChange: (product: string, up: boolean) => void;
    product: string;
}

export default class OrderBook extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        this.handleGroupChange = this.handleGroupChange.bind(this);
    }

    getControlStyle(up: boolean): React.CSSProperties {
        let disabled = false;

        if ((up && this.props.maxGroup === this.props.group) || (!up && this.props.minGroup === this.props.group)) {
            disabled = true;
        }

        return {
            opacity: disabled ? .25 : 1,
            cursor: disabled ? 'not-allowed' : 'pointer'
        }
    }

    handleGroupChange(up: boolean) {
        this.props.onGroupChange(this.props.product, up)
    }

    render() {
        let toDecimal = new Intl.NumberFormat('en-US', {
            style: 'decimal',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        });

        let toWhole = new Intl.NumberFormat('en-US', {
            style: 'decimal',
            maximumFractionDigits: 0
        });

        let spread = parseFloat(Object.keys(this.props.asks).sort().slice(0, 1)[0]) - parseFloat(Object.keys(this.props.bids).sort().reverse().slice(0, 1)[0]);

        return (
            <div className="Orderbook">
                <div className="Orderbook-controls">
                    <span>
                        Spread {
                        toDecimal.format(spread)
                    } ({
                        toDecimal.format(spread / parseFloat(Object.keys(this.props.asks).sort().slice(0, 1)[0]) * 100)
                    }%)
                    </span>
                    <span>
                        Group {toDecimal.format(this.props.group)} &nbsp;
                    </span>
                    <span>
                        <button className="Orderbook-control" style={this.getControlStyle(false)} onClick={(e) => this.handleGroupChange(false)}>-</button>
                        <button className="Orderbook-control" style={this.getControlStyle(true)} onClick={(e) => this.handleGroupChange(true)}>+</button>
                    </span>
                </div>
                <table>
                    <thead>
                        <tr>
                            <th colSpan={3}>{this.props.product}</th>
                        </tr>
                        <tr>
                            <th className="left-align">Price</th>
                            <th>Size</th>
                            <th>Total</th>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            Object.keys(this.props.asks).sort().slice(0, 5).reverse().map((level) =>
                                <tr key={level}>
                                    <td className="Orderbook-ask left-align">{toDecimal.format(parseFloat(level))}</td>
                                    <td>{toWhole.format(this.props.asks[level].size)}</td>
                                    <td>{toWhole.format(this.props.asks[level].total)}</td>
                                </tr>
                            )
                        }
                    </tbody>
                    <tbody>
                        {
                            Object.keys(this.props.bids).sort().reverse().slice(0, 5).map((level) =>
                                <tr key={level}>
                                    <td className="Orderbook-bid left-align">{toDecimal.format(parseFloat(level))}</td>
                                    <td>{toWhole.format(this.props.bids[level].size)}</td>
                                    <td>{toWhole.format(this.props.bids[level].total)}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
