# Sigma 36

## Introduction

**SIG**nal **M**atching, **THR**eshold **E**valuation and **E**stimation, and **S**trike **I**ndicator **X**trapolation

This project doesn't quite live up to the promise of its anagram...yet.  For now, the site will serve up a configurable orderbook based on a public websocket feed.  It can be configured to display as many products as the feed reports, and bids and asks can be grouped at configurable price levels.

## Setup

The project is built with Create-React-App.  Once the repository has been pulled down, there are two options for either local development or production building.

1. Use the included script to create a Docker environment.
2. Rely on local node and npm libraries.

### ./sigma36

This requires `docker` and `docker-compose`.

`./sigma36 setup` will create the Docker image used to run the commands.  This step is automatically run if the image isn't built.

`./sigma36 start` will compile and run the project in development mode with watchers to automatically recompile and refresh the browser view as changes are made.

`./sigma36 test` will run the test suite.

`./sigma36 build` will create production optimized static files in `./build`. The default service path is `/` unless changed using the `homepage` field in `./package.json`.

### npm

This requires `node` and `npm`.

`npm start` will compile and run the project in development mode with watchers to automatically recompile and refresh the browser view as changes are made.

`npm test` will run the test suite.

`npm run build` will create production optimized static files in `./build`. The default service path is `/` unless changed using the `homepage` field in `./package.json`.

### Configuration

There are a handful of configuration directs in the `./.env` file.  Here there are listed with their default values

* REACT_APP_FEED_URL=wss://www.cryptofacilities.com/ws/v1
    * This is the websocket address for the feed data.
* REACT_APP_PRODUCTS=PI_XBTUSD
    * A comma separated list of products to show
* REACT_APP_GROUPS=10
    * A comma separated list of default price groupings for the products listed in `REACT_APP_PRODUCTS`.
* REACT_APP_INCREMENTS=.5,1,2.5,5,10,25,50,100,250,500,1000,2500
    * A comma separated list of price grouping increments to use for all products
