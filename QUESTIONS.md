# Questions

1. ### What would you add to your solution if you had more time?

I would write some more tests.  The functionality requested exists and works, but I only wrote one simple test to verify that the page renders, and a handful of tests for the core price level grouping logic.

2. ### What would you have done differently if you knew this page was going to get thousands of views per second vs per week?

Coding-wise I would not change much, but production deployment concerns are many.  These are served as static files so using a good load balancing infrastructure would be key.  Otherwise, all the "hard" work is client-side.

3. ### What was the most useful feature that was added to the latest version of your chosen language?  Please include a snippet of code that shows how you've used it.

I might object to "chosen language", as I was informed that the solution had to be written with React and Typescript.

4. ### How would you track down a performance issue in production?  Have you ever had to do this?

The first step to tracking down any issue is to really understand how what is being reported is related to what is actually happening.  Often the description of the issue provided by an end user is vague, imprecise, or lacking salient details.  Once a good understanding of the issue has been obtained, the next step is to replicate the issue, ideally in production, if possible, or in a staging or local environment otherwise.
   
I have tracked down many performance issues in production over the years.  Most causes tend to fall into a handful of categories: slow or inefficient database queries, preventable high memory consumption or memory leakage, or, finally, improperly configured networking or routing.  Of course, there's always just poorly written code that presents as a performance issue but is really just a mistake in assumptions or conclusions made by the developer.

5. ### Can you describe common security concerns to consider for a frontend developer?

Of course, there's the OWASP Top 10 items like injection, sensitive data exposure, XSS, CSRF, and broken authentication/authorization.  Additionally, there are basic caveats for using up-to-date dependencies and auditing third party libraries.

6. ### How would you improve the Crypto Facilities API that you just used?
   
 This was a trick question, so I modified it to be answerable. ;-)  It was difficult to find subscription information in the documentation for the Websocket API.  As for the API itself, although I did browse the documentation, my interaction with it was limited to the requirements of the assessment.  I did not see anything that stood out as needing improvement in what I used.  
   